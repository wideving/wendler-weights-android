package com.wideving.wendler.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class AccessoryExercise(
    val name: String,
    val sets: Int,
    val reps: Int
) : Parcelable