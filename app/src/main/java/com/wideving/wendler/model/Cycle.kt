package com.wideving.wendler.model
import com.wideving.wendler.utility.Load

class Cycle(
    val workouts: List<Workout>,
    val load: Load
)