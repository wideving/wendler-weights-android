package com.wideving.wendler.model

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Exercise(
    val name: String,
    val weight: Double,
    val increment: Double
) : Parcelable