package com.wideving.wendler.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
@Entity(tableName = "workouts")
data class Workout(
    val mainExercise: Exercise,
    val accessoryExercises: List<AccessoryExercise>,
    @PrimaryKey(autoGenerate = true) var id: Int = 0
) : Parcelable {
}


