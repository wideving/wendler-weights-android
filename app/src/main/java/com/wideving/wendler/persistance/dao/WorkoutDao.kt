package com.wideving.wendler.persistance.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.wideving.wendler.model.Workout

@Dao
interface WorkoutDao {

    @Query("SELECT * FROM workouts")
    fun getAll(): LiveData<List<Workout>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWorkouts(vararg workout: Workout)

    @Delete
    fun delete(workout: Workout)
}