package com.wideving.wendler.persistance.database

import androidx.room.TypeConverter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.wideving.wendler.model.AccessoryExercise

class AccessoryExerciseConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun toAccessoryExercise(accessoryExerciseString: String): List<AccessoryExercise> {
            val list = Types.newParameterizedType(
                List::class.java,
                AccessoryExercise::class.java
            )

            val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
            val adapter: JsonAdapter<List<AccessoryExercise>> = moshi.adapter(list)
            return adapter.fromJson(accessoryExerciseString)!!
        }

        @JvmStatic
        @TypeConverter
        fun toJson(accessoryExercise: List<AccessoryExercise>): String {
            val list = Types.newParameterizedType(
                List::class.java,
                AccessoryExercise::class.java
            )

            val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
            val adapter = moshi.adapter<List<AccessoryExercise>>(list)
            return adapter.toJson(accessoryExercise)!!
        }
    }
}