package com.wideving.wendler.persistance.database

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.wideving.wendler.model.Exercise

class ExerciseConverter {
    companion object {
        @JvmStatic
        @TypeConverter
        fun toExercise(exerciseString: String): Exercise {
            val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
            val adapter = moshi.adapter(Exercise::class.java)
            return adapter.fromJson(exerciseString)!!
        }

        @JvmStatic
        @TypeConverter
        fun toJson(exercise: Exercise): String {
            val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
            val adapter = moshi.adapter(Exercise::class.java)
            return adapter.toJson(exercise)
        }
    }
}