package com.wideving.wendler.persistance.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.wideving.wendler.model.Workout
import com.wideving.wendler.persistance.dao.WorkoutDao

@Database(
    entities = [Workout::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(ExerciseConverter::class, AccessoryExerciseConverter::class)
abstract class WendlerDatabase : RoomDatabase() {
    abstract fun workoutDao(): WorkoutDao
}