package com.wideving.wendler.persistance.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.wideving.wendler.model.Workout
import com.wideving.wendler.persistance.database.WendlerDatabase


interface WorkoutRepository {
    val hasWorkouts: LiveData<Boolean>
    val workouts: LiveData<List<Workout>>
    fun addWorkout(workout: Workout)
    fun updateWorkout(workout: Workout)
    fun removeWorkout(workout: Workout)
}

class WorkoutRepositoryImpl(
    private val database: WendlerDatabase
) : WorkoutRepository {
    override val hasWorkouts: LiveData<Boolean>
        get() = workouts.map { it.isNotEmpty() }

    override val workouts: LiveData<List<Workout>>
        get() = database.workoutDao().getAll()

    override fun updateWorkout(workout: Workout) {
        database.workoutDao().insertWorkouts(workout)
    }

    override fun addWorkout(workout: Workout) {
        database.workoutDao().insertWorkouts(workout)
    }


    override fun removeWorkout(workout: Workout) {
        database.workoutDao().delete(workout)
    }
}