package com.wideving.wendler.ui.add

import com.wideving.wendler.R
import com.wideving.wendler.model.AccessoryExercise
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.view_accessory_exercise_large.view.*

class AccessoryExerciseItem(
    private val accessoryExercise: AccessoryExercise
): Item() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            name.text = accessoryExercise.name
            sets.text = accessoryExercise.sets.toString()
            reps.text = accessoryExercise.reps.toString()
        }
    }
    override fun getLayout(): Int = R.layout.view_accessory_exercise_large
}