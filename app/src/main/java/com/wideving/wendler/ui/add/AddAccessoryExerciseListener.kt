package com.wideving.wendler.ui.add

import com.wideving.wendler.model.AccessoryExercise

interface AddAccessoryExerciseListener {
    fun add(exercise: AccessoryExercise)
}