package com.wideving.wendler.ui.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.text.isDigitsOnly
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.wideving.wendler.R
import com.wideving.wendler.model.AccessoryExercise
import kotlinx.android.synthetic.main.fragment_add_accessory_exercise.*


class AddAccessoryFragment : BottomSheetDialogFragment() {
    var listener: AddAccessoryExerciseListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val contextThemeWrapper = ContextThemeWrapper(activity, R.style.Theme_Wendler)
        return inflater.cloneInContext(contextThemeWrapper)
            .inflate(R.layout.fragment_add_accessory_exercise, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        repsEditText.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                addExerciseButton.callOnClick()
                return@setOnEditorActionListener true
            }
            false
        }

        assistanceExerciseEditText.setOnKeyListener { _, _, _ ->
            if (validName()) {
                assistanceExerciseEditText.error = null
            }
            false
        }

        repsEditText.setOnKeyListener { _, _, _ ->
            if (validReps()) {
                repsEditText.error = null
            }
            false
        }

        setsEditText.setOnKeyListener { _, _, _ ->
            if (validSets()) {
                setsEditText.error = null
            }
            false
        }

        addExerciseButton.setOnClickListener {
            if (!validName()) {
                assistanceExerciseTextInput.error = getString(R.string.validation_error_name)
            }

            if (!validSets()) {
                setsTextInput.error =
                    getString(R.string.validation_error_more_then_zero)
            }

            if (!validReps()) {
                repsTextInput.error =
                    getString(R.string.validation_error_more_then_zero)
            }

            if (validName() && validSets() && validReps()) {
                listener?.add(
                    AccessoryExercise(
                        assistanceExerciseEditText.text.toString(),
                        setsEditText.text.toString().toIntOrNull() ?: 0,
                        repsEditText.text.toString().toIntOrNull() ?: 0
                    )
                )
                dismiss()
            }
        }
    }

    private fun validName() = assistanceExerciseEditText.text?.isNotEmpty() ?: false
    private fun validSets(): Boolean {
        val text = setsEditText.text ?: ""
        return text.isNotEmpty() && text.isDigitsOnly()
    }

    private fun validReps(): Boolean {
        val text = setsEditText.text ?: ""
        return text.isNotEmpty() && text.isDigitsOnly()
    }
}