package com.wideving.wendler.ui.add

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.wideving.wendler.R
import com.wideving.wendler.model.AccessoryExercise
import com.wideving.wendler.model.Workout
import com.wideving.wendler.utility.Keys
import com.wideving.wendler.utility.MarginDecoration
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_add.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class AddFragment : Fragment(),
    AddAccessoryExerciseListener {
    companion object {
        const val WORKOUT_KEY = "workout"
        fun newInstance(workout: Workout? = null): AddFragment {
            val fragment = AddFragment()
            workout?.let {
                val bundle = Bundle()
                bundle.putParcelable(WORKOUT_KEY, workout)
                fragment.arguments = bundle
            }

            return fragment
        }
    }

    private val viewModel: AddViewModel by viewModel()
    private val groupAdapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val preferences = PreferenceManager.getDefaultSharedPreferences(view.context)

        val smallIncrement = preferences.getString(Keys.smallIncrement, "2.5")
        val largeIncrement = preferences.getString(Keys.largeIncrement, "5.0")
        val unitOfMass =
            preferences.getString(Keys.unitOfMass, "kg")?.toLowerCase(Locale.getDefault())

        smallIncrementRadio.text =
            getString(R.string.weight, smallIncrement!!.toDouble(), unitOfMass)

        largeIncrementRadio.text =
            getString(R.string.weight, largeIncrement!!.toDouble(), unitOfMass)


        val workout = viewModel.addWorkout(arguments?.getParcelable(WORKOUT_KEY))

        if (workout != null) {
            with(workout.mainExercise) {
                mainExerciseEditText.setText(name)
                oneRmEditText.setText(weight.toString())
                radioGroup.check(if (increment == smallIncrement.toDouble())
                    R.id.smallIncrementRadio else R.id.largeIncrementRadio)
            }

            workout.accessoryExercises.forEach {
                viewModel.addAccessoryExercise(it)
            }

            (activity as AppCompatActivity).supportActionBar?.title =
                getString(R.string.add_workout_title_edit)
        } else {
            (activity as AppCompatActivity).supportActionBar?.title =
                getString(R.string.add_workout_title)
        }

        mainExerciseEditText.setOnKeyListener { _, _, _ ->
            if (validName()) {
                mainExerciseTextInput.error = null
            }
            false
        }

        oneRmEditText.setOnKeyListener { _, _, _ ->
            if (validOneRm()) {
                oneRmTextInput.error = null
            }
            false
        }

        recyclerView.addItemDecoration(
            MarginDecoration(
                resources.getDimension(
                    R.dimen.default_spacing
                ).toInt()
            )
        )

        fab.setOnClickListener {
            val fragment = AddAccessoryFragment()
            fragment.listener = this
            fragment.show(parentFragmentManager, null)
        }

        recyclerView.adapter = groupAdapter

        viewModel.accessoryExercises.observe(viewLifecycleOwner, Observer { exercises ->
            val exercisesGroup = exercises.map {
                AccessoryExerciseItem(it)
            }
            groupAdapter.update(exercisesGroup)
        })

        val itemTouchHelper = ItemTouchHelper(object :
            ItemTouchHelper.SimpleCallback(
                0,
                ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
            ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                viewModel.removeAccessoryExercise(viewHolder.adapterPosition)
            }
        })

        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_add, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                parentFragmentManager.popBackStack()
            }
            R.id.menu_save -> {
                save()
            }
        }
        return true
    }

    private fun save() {
        if (!validName()) {
            mainExerciseTextInput.error = getString(R.string.validation_error_name)
        }

        if (!validOneRm()) {
            oneRmTextInput.error = getString(R.string.validation_one_rep_max)
        }

        if (validName() && validOneRm()) {

            val preferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
            val smallIncrement = preferences.getString(Keys.smallIncrement, "2.5")
            val largeIncrement = preferences.getString(Keys.largeIncrement, "5.0")

            val name = mainExerciseEditText.text.toString()
            val oneRm = oneRmEditText.text.toString().toDouble()
            val increment = when (radioGroup.checkedRadioButtonId) {
                R.id.smallIncrementRadio -> smallIncrement!!.toDouble()
                else -> largeIncrement!!.toDouble()
            }
            viewModel.save(name, oneRm, increment)
            parentFragmentManager.popBackStack()
        }
    }

    override fun add(exercise: AccessoryExercise) {
        viewModel.addAccessoryExercise(exercise)
    }

    private fun validName(): Boolean {
        return (mainExerciseEditText.text?.toString() ?: "").isNotEmpty()
    }

    private fun validOneRm(): Boolean {
        val text = oneRmEditText.text?.toString() ?: ""
        return text.toIntOrNull() != null || text.toDoubleOrNull() != null
    }
}