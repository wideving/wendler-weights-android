package com.wideving.wendler.ui.add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wideving.wendler.model.AccessoryExercise
import com.wideving.wendler.model.Exercise
import com.wideving.wendler.model.Workout
import com.wideving.wendler.persistance.repository.WorkoutRepository
import java.util.concurrent.Executors

class AddViewModel(
    private val repository: WorkoutRepository
) : ViewModel() {

    private var workout: Workout? = null
    private val ioExecutor = Executors.newSingleThreadExecutor()
    private val _accessoryExercises: MutableLiveData<List<AccessoryExercise>> = MutableLiveData()
    val accessoryExercises: LiveData<List<AccessoryExercise>>
        get() = _accessoryExercises

    // If workout is not null user is editing a workout
    fun addWorkout(workout: Workout?): Workout? {
        this.workout = workout
        return workout
    }

    fun addAccessoryExercise(exercise: AccessoryExercise) {
        val exercises = _accessoryExercises.value?.toMutableList() ?: mutableListOf()
        exercises.add(exercise)
        _accessoryExercises.value = exercises
    }

    fun removeAccessoryExercise(position: Int) {
        val exercises = _accessoryExercises.value?.toMutableList() ?: mutableListOf()
        exercises.removeAt(position)
        _accessoryExercises.value = exercises
    }

    fun save(name: String, oneRm: Double, increment: Double) {
        if (workout != null) {
            workout?.let {
                val modifiedWorkout = it.copy(
                    mainExercise = it.mainExercise.copy(
                        name = name,
                        weight = oneRm,
                        increment = increment
                    ),
                    accessoryExercises = accessoryExercises.value ?: emptyList()
                )
                ioExecutor.execute {
                    repository.addWorkout(modifiedWorkout)
                }
            }
        } else {
            val newWorkout = Workout(
                Exercise(name, oneRm, increment),
                _accessoryExercises.value ?: emptyList()
            )

            ioExecutor.execute {
                repository.addWorkout(newWorkout)
            }
        }
    }
}