package com.wideving.wendler.ui.app

import androidx.room.Room
import com.wideving.wendler.persistance.database.WendlerDatabase
import com.wideving.wendler.persistance.repository.WorkoutRepository
import com.wideving.wendler.persistance.repository.WorkoutRepositoryImpl
import com.wideving.wendler.ui.add.AddViewModel
import com.wideving.wendler.ui.cycle.CycleViewModel
import com.wideving.wendler.ui.main.MainViewModel
import com.wideving.wendler.ui.setup.SetupViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val module = module {
    single<WorkoutRepository> { WorkoutRepositoryImpl(get()) }
    single {
        Room.databaseBuilder(
            androidApplication(),
            WendlerDatabase::class.java,
            "wendler-database"
        ).build()
    }

    viewModel { AddViewModel(get()) }
    viewModel { CycleViewModel(get()) }
    viewModel { SetupViewModel(get()) }
    viewModel { MainViewModel(get()) }

}
