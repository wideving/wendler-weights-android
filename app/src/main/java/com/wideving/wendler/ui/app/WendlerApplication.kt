package com.wideving.wendler.ui.app

import android.app.Application
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.wideving.wendler.utility.Keys
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WendlerApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@WendlerApplication)
            modules(module)
        }

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        //Set default values if first launch
        if (preferences.getString(Keys.smallIncrement, "Not set") == "Not set") {
            preferences.edit {
                putString(Keys.smallIncrement, "2.5")
                putString(Keys.largeIncrement, "5.0")
                putString(Keys.unitOfMass, "kg")
            }
        }
    }
}