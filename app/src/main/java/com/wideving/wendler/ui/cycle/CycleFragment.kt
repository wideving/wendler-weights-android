package com.wideving.wendler.ui.cycle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.wideving.wendler.R
import com.wideving.wendler.ui.cycle.adapter.WorkoutItem
import com.wideving.wendler.utility.Load
import com.wideving.wendler.utility.MarginDecoration
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_cycle.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CycleFragment : Fragment() {
    private val viewModel: CycleViewModel by viewModel()
    private val groupAdapter = GroupAdapter<GroupieViewHolder>()

    companion object {
        const val loadKey = "LOAD"
        fun newInstance(load: Load): CycleFragment {
            return CycleFragment().also {
                val bundle = Bundle()
                bundle.putSerializable(loadKey, load)
                it.arguments = bundle
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        (arguments?.getSerializable(loadKey) as? Load)?.let {
            viewModel.setLoad(it)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_cycle,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = groupAdapter
        recyclerView.addItemDecoration(
            MarginDecoration(
                resources.getDimension(
                    R.dimen.default_spacing
                ).toInt()
            )
        )
        viewModel.cycle.observe(viewLifecycleOwner, Observer { cycle ->
            val cyclesGroup = cycle.workouts.map {
                WorkoutItem(it, cycle.load)
            }

            groupAdapter.update(cyclesGroup, true)
        })
    }
}