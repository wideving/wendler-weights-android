package com.wideving.wendler.ui.cycle


import androidx.lifecycle.*
import com.wideving.wendler.model.Cycle
import com.wideving.wendler.persistance.repository.WorkoutRepository
import com.wideving.wendler.utility.Load

class CycleViewModel(
    private val repository: WorkoutRepository
) : ViewModel() {
    private val load: MutableLiveData<Load> = MutableLiveData()

    val cycle: LiveData<Cycle> = load.switchMap { load ->
        repository.workouts.map { workouts ->
            Cycle(workouts, load)
        }
    }

    fun setLoad(load: Load) {
        this.load.value = load
    }
}
