package com.wideving.wendler.ui.cycle.adapter

import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updateMargins
import androidx.preference.PreferenceManager
import com.wideving.wendler.R
import com.wideving.wendler.model.Workout
import com.wideving.wendler.utility.Keys
import com.wideving.wendler.utility.Load
import com.wideving.wendler.utility.LoadManager
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.view_accessory_exercise.view.*
import kotlinx.android.synthetic.main.view_main_exercise.view.*
import kotlinx.android.synthetic.main.view_main_exercise.view.name
import kotlinx.android.synthetic.main.view_workout.view.*

class WorkoutItem(
    private val workout: Workout,
    private val load: Load
) : Item() {
    override fun getLayout(): Int {
        return R.layout.view_workout
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)

            name.text = workout.mainExercise.name
            setOneReps.text = load.reps[0]
            setTwoReps.text = load.reps[1]
            setThreeReps.text = load.reps[2]

            val weights = LoadManager.calculateWeights(
                load,
                workout.mainExercise,
                preferences.getString(Keys.smallIncrement, "2.5")!!.toDouble()
            )

            val unitOfMass = preferences.getString(Keys.unitOfMass, "KG")

            setOneWeight.text = context.getString(R.string.weight, weights[0], unitOfMass)
            setTwoWeight.text = context.getString(R.string.weight, weights[1], unitOfMass)
            setThreeWeight.text = context.getString(R.string.weight, weights[2], unitOfMass)

            val margin = resources.getDimension(R.dimen.default_spacing)


            if (workout.accessoryExercises.isEmpty()) {
                accessoryExercisesTitle.isVisible = false
                assistanceLayout.isVisible = false
            } else {
                workout.accessoryExercises.forEach { exercise ->
                    val view = LayoutInflater.from(context).inflate(
                        R.layout.view_accessory_exercise,
                        assistanceLayout,
                        false
                    ).also {
                        it.name.text = exercise.name
                        it.sets.text = exercise.sets.toString()
                        it.reps.text = exercise.reps.toString()
                        it.updateLayoutParams<LinearLayout.LayoutParams> {
                            if (workout.accessoryExercises.first() != exercise) {
                                updateMargins(
                                    top = margin.toInt()
                                )
                            }
                        }
                    }
                    assistanceLayout.addView(view)
                }
            }
        }
    }

    override fun unbind(viewHolder: GroupieViewHolder) {
        super.unbind(viewHolder)
        with(viewHolder.itemView) {
            assistanceLayout.removeAllViews()
            accessoryExercisesTitle.isVisible = false
            assistanceLayout.isVisible = false
        }
    }

}