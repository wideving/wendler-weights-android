package com.wideving.wendler.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.wideving.wendler.R
import kotlinx.android.synthetic.main.app_bar_layout.*

interface Router {
    fun navigateTo(fragment: Fragment, addToBackStack: Boolean)
}

class MainActivity : AppCompatActivity(), Router {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        navigateTo(MainFragment.newInstance(), false)
    }

    override fun navigateTo(fragment: Fragment, addToBackStack: Boolean) {
        val fragmentTransaction = supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainerView, fragment, null)

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null)
        }

        fragmentTransaction.commit()
    }
}
