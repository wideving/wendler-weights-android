package com.wideving.wendler.ui.main

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import com.wideving.wendler.R
import com.wideving.wendler.ui.cycle.CycleFragment
import com.wideving.wendler.ui.preferences.PreferencesFragment
import com.wideving.wendler.ui.setup.SetupFragment
import com.wideving.wendler.utility.Load
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {
    private val viewModel: MainViewModel by viewModel()

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_main,
            container,
            false
        )
    }

    //Todo supporta lbs
    //Todo Nästa setupitem?

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_preferences ->
                (activity as Router).navigateTo(
                    PreferencesFragment.newInstance(),
                    true
                )
        }
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.run {
            title = getString(R.string.app_name)
            setDisplayHomeAsUpEnabled(false)
            setHomeButtonEnabled(false)
        }

        tabLayout.setupWithViewPager(viewPager)
        viewPager.adapter = object : FragmentPagerAdapter(
            childFragmentManager,
            BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        ) {
            override fun getItem(position: Int): Fragment {
                val load: Load = when (position) {
                    0 -> Load.LIGHT
                    1 -> Load.MEDIUM
                    2 -> Load.HEAVY
                    3 -> Load.DELOAD
                    else -> Load.LIGHT
                }

                return CycleFragment.newInstance(load)
            }

            override fun getCount(): Int {
                return 4
            }

            override fun getPageTitle(position: Int): CharSequence? {
                return when (position) {
                    0 -> getString(R.string.light)
                    1 -> getString(R.string.medium)
                    2 -> getString(R.string.heavy)
                    3 -> getString(R.string.deload)
                    else -> ""
                }
            }
        }

        fab.setOnClickListener {
            (activity as Router)
                .navigateTo(SetupFragment.newInstance(), true)
        }

        viewModel.hasWorkouts.observe(viewLifecycleOwner, Observer { hasWorkouts ->
            tabLayout.isVisible = hasWorkouts
            emptyView.isVisible = !hasWorkouts
        })
    }
}