package com.wideving.wendler.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.wideving.wendler.persistance.repository.WorkoutRepository

class MainViewModel(
    private val repository: WorkoutRepository
) : ViewModel() {
    val hasWorkouts: LiveData<Boolean> = repository.hasWorkouts
}