package com.wideving.wendler.ui.preferences

import android.content.SharedPreferences
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import com.wideving.wendler.R
import com.wideving.wendler.utility.Keys

class PreferencesFragment : PreferenceFragmentCompat() {
    companion object {
        fun newInstance(): PreferencesFragment {
            return PreferencesFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

    private val listener =
        SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
            if (key == Keys.USE_LBS) {
                if (sharedPreferences.getBoolean(Keys.USE_LBS, false)) {
                    sharedPreferences.edit {
                        putString(Keys.smallIncrement, "5.0")
                        putString(Keys.largeIncrement, "10.0")
                        putString(Keys.unitOfMass, getString(R.string.lbs))
                    }
                } else {
                    sharedPreferences.edit {
                        putString(Keys.smallIncrement, "2.5")
                        putString(Keys.largeIncrement, "5.0")
                        putString(Keys.unitOfMass, getString(R.string.kg))
                    }
                }
            }
        }

    override fun onStart() {
        super.onStart()
        PreferenceManager.getDefaultSharedPreferences(requireContext())
            .registerOnSharedPreferenceChangeListener(listener)
    }

    override fun onStop() {
        PreferenceManager.getDefaultSharedPreferences(requireContext())
            .unregisterOnSharedPreferenceChangeListener(listener)
        super.onStop()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> parentFragmentManager.popBackStack()
        }
        return true
    }
}