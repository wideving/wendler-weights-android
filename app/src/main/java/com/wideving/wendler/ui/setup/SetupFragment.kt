package com.wideving.wendler.ui.setup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.wideving.wendler.R
import com.wideving.wendler.model.Workout
import com.wideving.wendler.ui.add.AddFragment
import com.wideving.wendler.ui.main.Router
import com.wideving.wendler.ui.setup.adapter.ModifyWorkoutListener
import com.wideving.wendler.ui.setup.adapter.SetupWorkoutItem
import com.wideving.wendler.utility.ItemRemovedListener
import com.wideving.wendler.utility.MarginDecoration
import com.wideving.wendler.utility.SwipeToRemoveHelper
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.fragment_setup.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SetupFragment : Fragment(), ModifyWorkoutListener {
    private val viewModel: SetupViewModel by viewModel()
    private val groupAdapter = GroupAdapter<GroupieViewHolder>()

    companion object {
        fun newInstance(): SetupFragment {
            return SetupFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_setup,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.run {
            title = getString(R.string.setup_title)
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }

        recyclerView.run {
            adapter = groupAdapter
            addItemDecoration(
                MarginDecoration(
                    resources.getDimension(
                        R.dimen.default_spacing
                    ).toInt()
                )
            )
        }

        val swipeToRemoveHelper = SwipeToRemoveHelper(object : ItemRemovedListener {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val item = groupAdapter.getItem(viewHolder.adapterPosition) as SetupWorkoutItem
                viewModel.removeWorkout(item.workout)
            }
        })

        ItemTouchHelper(swipeToRemoveHelper).attachToRecyclerView(recyclerView)

        viewModel.workouts.observe(viewLifecycleOwner, Observer { workouts ->
            emptyView.isVisible = workouts.isEmpty()
            val groups = workouts.map { SetupWorkoutItem(it, this) }
            groupAdapter.update(groups, true)

        })

        fab.setOnClickListener {
            (activity as Router)
                .navigateTo(AddFragment.newInstance(), true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> parentFragmentManager.popBackStack()
        }
        return true
    }

    override fun modified(workout: Workout) {
        viewModel.updateWorkout(workout)
    }

    override fun edit(workout: Workout) {
        (activity as Router).navigateTo(
            AddFragment.newInstance(workout),
            true
        )
    }
}