package com.wideving.wendler.ui.setup

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.wideving.wendler.model.Workout
import com.wideving.wendler.persistance.repository.WorkoutRepository

import java.util.concurrent.Executors

class SetupViewModel(
    private val repository: WorkoutRepository
) : ViewModel() {
    private val ioExecutor = Executors.newSingleThreadExecutor()
    val workouts: LiveData<List<Workout>> = repository.workouts

    fun updateWorkout(workout: Workout) {
        ioExecutor.execute {
            repository.updateWorkout(workout)
        }
    }

    fun removeWorkout(workout: Workout) {
        ioExecutor.execute {
            repository.removeWorkout(workout)
        }
    }
}