package com.wideving.wendler.ui.setup.adapter

import com.wideving.wendler.model.Workout

interface ModifyWorkoutListener {
    fun modified(workout: Workout)
    fun edit(workout: Workout)
}