package com.wideving.wendler.ui.setup.adapter

import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.view.updateMargins
import androidx.preference.PreferenceManager
import com.wideving.wendler.R
import com.wideving.wendler.model.Workout
import com.wideving.wendler.utility.Keys
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.view_accessory_exercise.view.*
import kotlinx.android.synthetic.main.view_setup_workout.view.*
import kotlinx.android.synthetic.main.view_setup_workout.view.name
import java.util.*

class SetupWorkoutItem(
    val workout: Workout,
    private val listener: ModifyWorkoutListener
) : Item() {
    override fun getLayout(): Int {
        return R.layout.view_setup_workout
    }

    private fun modifyWorkout(increase: Boolean): Workout {
        val (_, weight, increment) = workout.mainExercise
        val newWeight = if (increase) weight.plus(increment) else weight.minus(increment)
        return workout.copy(mainExercise = workout.mainExercise.copy(weight = newWeight))
    }

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            val preferences = PreferenceManager.getDefaultSharedPreferences(context)
            val unitOfMass = if (preferences.getBoolean(
                    Keys.USE_LBS,
                    false
                )
            ) context.getString(R.string.lbs) else context.getString(R.string.kg)
            name.text = workout.mainExercise.name
            oneRm.text = context.getString(
                R.string.weight,
                workout.mainExercise.weight,
                unitOfMass.toUpperCase(Locale.getDefault())
            )
            increment.text = context.getString(
                R.string.increment,
                workout.mainExercise.increment,
                unitOfMass.toLowerCase(Locale.getDefault())
            )

            val margin = resources.getDimension(R.dimen.default_spacing)

            increaseButton.setOnClickListener {
                listener.modified(modifyWorkout(true))
            }

            decreaseButton.setOnClickListener {
                listener.modified(modifyWorkout(false))
            }

            editButton.setOnClickListener {
                listener.edit(workout)
            }

            if (workout.accessoryExercises.isEmpty()) {
                divider.isVisible = false
            } else {
                workout.accessoryExercises.forEach { exercise ->
                    val view = LayoutInflater.from(context).inflate(
                        R.layout.view_accessory_exercise,
                        assistanceLayout,
                        false
                    ).also {
                        it.name.text = exercise.name
                        it.sets.text = exercise.sets.toString()
                        it.reps.text = exercise.reps.toString()
                        it.updateLayoutParams<LinearLayout.LayoutParams> {
                            if (workout.accessoryExercises.first() != exercise) {
                                updateMargins(
                                    top = margin.toInt()
                                )
                            }
                        }
                    }
                    assistanceLayout.addView(view)
                }
            }
        }
    }

    override fun unbind(viewHolder: GroupieViewHolder) {
        super.unbind(viewHolder)
        with(viewHolder.itemView) {
            divider.isVisible = true
            assistanceLayout.removeAllViews()
        }
    }
}