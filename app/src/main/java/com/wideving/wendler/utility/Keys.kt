package com.wideving.wendler.utility

object Keys {
    const val USE_LBS = "useLbs"
    const val smallIncrement = "smallIncrement"
    const val largeIncrement = "largeIncrement"
    const val unitOfMass = "unitOfMass"
}