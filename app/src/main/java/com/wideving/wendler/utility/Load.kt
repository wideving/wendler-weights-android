package com.wideving.wendler.utility

enum class Load(val percentages: List<Double>, val reps: List<String>) {
    LIGHT(listOf(0.65, 0.75, 0.85), listOf("5", "5", "5+")),
    MEDIUM(listOf(0.70, 0.80, 0.90), listOf("3", "3", "3+")),
    HEAVY(listOf(0.75, 0.85, 0.95), listOf("5", "3", "1+")),
    DELOAD(listOf(0.40, 0.50 ,0.60), listOf("5", "5", "5"))
}