package com.wideving.wendler.utility

import com.wideving.wendler.model.Exercise
import kotlin.math.floor

object LoadManager {
    fun calculateWeights(load: Load, exercise: Exercise, rounding: Double): List<Double> {
        return load.percentages.map { percentage ->
            floor((exercise.weight * percentage) / rounding) * rounding
        }
    }
}